# ER1243
Ce dossier fournit les programmes de l'*Études et résultats* N°&nbsp;1243 de la DREES : [« Les maladies chroniques touchent plus souvent les personnes modestes et réduisent davantage leur espérance de vie »](https://drees.solidarites-sante.gouv.fr/publications-communique-de-presse/etudes-et-resultats/les-maladies-chroniques-touchent-plus-souvent), 06/10/2022.
Ces programmes génèrent automatiquement le texte de la publication avec les chiffres ajustés, le tableur qui l'accompagne et les indicateurs supplémentaires publiées sur [data.drees](https://data.drees.solidarites-sante.gouv.fr/explore/dataset/er_inegalites_maladies_chroniques/information/).

En plus des programmes, le dossier contient La [publication elle-même](ER1243_MAJ.pdf) et le [tableur](ER1243.xlsx).

L'ensemble des programmes mis à disposition dans le dossier *01_scripts* se partage en deux blocs :

### 1. Reconstitution de la cartographie des pathologies
<span style="color:grey">*(temps d'exécution : au mieux 2 heures avec 40 cœurs de processeurs)*</span>

⚠ *Nécessite le logiciel Spark*

Un paquet dédié à la production de la cartographie, plus souple et lisible que cet enchaînement de scripts, sera partagé ultérieurement et sera signalé sur cette page.

- [*methodes_carto_pathos_G7_pour_R.xlsx*](01_scripts/methodes_carto_pathos_G7_pour_R.xlsx) : tableur Excel qui décrit tous les algorithmes de la cartographie
- [*00_main.R*](01_scripts/00_main.R) : programme maître qui exécute les scripts de 00 à 04
- [*00_utils.R*](01_scripts/00_utils.R) : fonctions généralistes dont certaines sont issues d'un paquet *redpsante* utilisé à la DREES
- [*01_chargement.R*](01_scripts/01_chargement.R) : objets nécessaires à la production de la cartographie
- [*02_production_fonctions.R*](01_scripts/02_production_fonctions.R) : fonctions nécessaires à la production de la cartographie 
- [*03_production.R*](01_scripts/03_production.R) : production de la cartographie des pathologies
- [*04_comparaison.R*](01_scripts/04_comparaison.R) : comparaison du résultat de *03_production.R* avec les tops patho de la CNAM pour le régime général

### 2. Statistiques de l'ER

<span style="color:grey">*(temps d'exécution : au mieux 2 heures et 30 minutes avec 40 cœurs de processeurs)*</span>

⚠ *Nécessite une table de synthèse des données de l'EDP, appelée `individu_S` et produite par la Drees*

- [*05_chargement_final.R*](01_scripts/05_chargement_final.R) : chargement des tables produites par *03_production.R* et calcul des statistiques de l'ER (incidences, prévalences, espérance de vie...)
- [*07_ER_biblio.bib*](01_scripts/07_ER_biblio.bib) : références bibliographiques de l'ER
- [*07_ER_topPatho.Rmd*](01_scripts/07_ER_topPatho.Rmd) : notebook qui génère la publication ainsi que le tableur et les indicateurs sur data.drees
  - [*07_ER_topPatho20221007.docx*](01_scripts/07_ER_topPatho20221007.docx) : la publication générée par le notebook et donc avant maquettage
  - [*07_ER_topPatho20221007.xlsx*](01_scripts/07_ER_topPatho20221007.xlsx) : le tableur généré par le notebook et donc avant retouches manuelles
- [*tuto_publi_style.docx*](01_scripts/tuto_publi_style.docx) : document word qui sert de modèle de style pour *07_ER_topPatho.Rmd*
- [*universite-de-montreal-apa.csl*](01_scripts/universite-de-montreal-apa.csl) : styles des citations bibliographiques (charte éditoriale de la DREES)

### Réplicabilité

Ce code source ne permettra sans doute pas à lui seul de répliquer le résultat dans la mesure où
- la reconstitution nécessite une installation Spark (avec SparkR) or Spark n'est pas disponible sur le portail SNDS de la CNAM. L'adaptation à une base Oracle devrait pouvoir se faire sans trop de difficultés (il faudra surtout ajuster les fonctions de *02_production_fonctions.R*) mais il serait préférable de partir du paquet dédié annoncé ci-dessus.
- il s'appuie sur une table de synthèse des données de l'EDP, appelée `individu_S` et produite par la DREES et dont le code source sera partagé lors de la prochaine production de l'EDP-Santé.

En revanche, ces programmes permettent de comprendre précisément tous les partis pris de l'étude et la manière dont sont faits les calculs. Certes, la méthode générale est décrite dans l'encadré 2 de la publication, mais rien ne vaut la lecture des codes pour s'ôter d'un doute.
Vive le partage des codes !
